public class Types {
    public static void main(String[] args) {
        double f = 5.1316;
        byte v = 127;
        System.out.println("double - переменная типа с плавающей точкой с наибольшим диапазоном чисел = " + f);
        System.out.print("byte - переменная целочисленного типа с наименьшим диапазоном чисел = " + v);
    }
}
